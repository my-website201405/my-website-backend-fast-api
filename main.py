from fastapi import FastAPI
from backend.helloworld.helloworld import helloworld_router

app = FastAPI()


app.include_router(helloworld_router)
