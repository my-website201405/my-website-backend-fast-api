# my-website-backend

## badges
(to be done)

![status](https://gitlab.com/my-website201405/my-website-backend-fast-api/badges/main/pipeline.svg?ignore_skipped=true)
![coverage](https://gitlab.com/my-website201405/my-website-backend-fast-api/badges/main/coverage.svg?job=coverage)
deployment status


## what is it?
in this repo I will put my website's backend code. this website will be:

a short presentation of me
an access to my resume
my portfolio
a call to action to a contact form


## which technologies I intend to use?

database via sqlalchemy ORM, maybe postgresql
backend with flask
frontend with vue


## how to set it up for development (maybe to make this project better?)
for instance, no instructions given

## how to use it in production environment?
for instance, no instruction given

## roadmap

- [] first things first: dev stuff first
    * [] dependencies
    * [] linting tools
    * [] testing suite
- [] API models:
    * school and professionnal experiences
    * portolio: online projects
    * and later, text for presentation, hobbies, etc... (those will be first written hardcoded in frontend)


## License
do whatever you want with this project as soon as it is not negative to anyone in any context.