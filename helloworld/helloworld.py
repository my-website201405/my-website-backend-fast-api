from fastapi import APIRouter

helloworld_router = APIRouter()


@helloworld_router.get("/helloworld")
async def root():
    return {"message": "Hello World"}
