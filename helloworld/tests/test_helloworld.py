from fastapi.testclient import TestClient
from fastapi import status
from unittest import TestCase

from backend.main import app

client = TestClient(app)


class HelloWorldTest(TestCase):
    
    def test_succeed(self):
        self.assertEqual(0, 0)

    def test_helloworld(self):
        response = client.get("/helloworld")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(),{
            "message":"Hello World",
        })

    